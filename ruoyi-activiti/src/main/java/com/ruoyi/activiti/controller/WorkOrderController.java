package com.ruoyi.activiti.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.ruoyi.activiti.domain.BizExampleDemo;
import com.ruoyi.activiti.service.IBizExampleDemoService;
import com.ruoyi.activiti.service.IProcessService;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.activiti.domain.WorkOrder;
import com.ruoyi.activiti.service.IWorkOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

import static com.ruoyi.common.core.domain.AjaxResult.error;
import static com.ruoyi.common.core.domain.AjaxResult.success;

/**
 * 工单流程Controller
 *
 * @author 丁宪明
 * @date 2020-12-22
 */
@RestController
@RequestMapping("/workorder/order")
public class WorkOrderController extends BaseController {
    @Autowired
    private IWorkOrderService workOrderService;
    @Autowired
    private IProcessService processService;

    /**
     * 查询工单流程列表？测试提交
     */
    @PreAuthorize("@ss.hasPermi('workorder:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(WorkOrder workOrder) {
        startPage();
        List<WorkOrder> list = workOrderService.selectWorkOrderList(workOrder);
        return getDataTable(list);
    }

    /**
     * 导出工单流程列表
     */
    @PreAuthorize("@ss.hasPermi('workorder:order:export')")
    @Log(title = "工单流程", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WorkOrder workOrder) {
        List<WorkOrder> list = workOrderService.selectWorkOrderList(workOrder);
        ExcelUtil<WorkOrder> util = new ExcelUtil<WorkOrder>(WorkOrder.class);
        return util.exportExcel(list, "order");
    }

    /**
     * 获取工单流程详细信息
     */
    @PreAuthorize("@ss.hasPermi('workorder:order:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(workOrderService.selectWorkOrderById(id));
    }

    /**
     * 新增工单流程
     */
    @PreAuthorize("@ss.hasPermi('workorder:order:add')")
    @Log(title = "工单流程", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WorkOrder workOrder) {
        return toAjax(workOrderService.insertWorkOrder(workOrder));
    }

    /**
     * 修改工单流程
     */
    @PreAuthorize("@ss.hasPermi('workorder:order:edit')")
    @Log(title = "工单流程", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WorkOrder workOrder) {
        return toAjax(workOrderService.updateWorkOrder(workOrder));
    }

    /**
     * 删除工单流程
     */
    @PreAuthorize("@ss.hasPermi('workorder:order:remove')")
    @Log(title = "工单流程", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(workOrderService.deleteWorkOrderByIds(ids));
    }

    /**
     * 提交申请
     */
    @Log(title = "示例Demo", businessType = BusinessType.UPDATE)
    @PostMapping("/submitApply/{id}")
    @ResponseBody
    public AjaxResult submitApply(@PathVariable Long id, String variablesStr) {
        try {
            System.out.println("variables: " + variablesStr);
            Map<String, Object> vars = (Map<String, Object>) JSON.parse(variablesStr);
            String[] users = vars.get("users").toString().split(",");
            WorkOrder bizExampleDemo = workOrderService.selectWorkOrderById(id);
            Map<String, Object> variables = new HashMap<>();
            if (users.length > 0) {
                Object value = Arrays.asList(users);
                variables.put("users", value);
            }
            variables.put("applyUserId", SecurityUtils.getUsername());
            processService.submitApply(bizExampleDemo, "workorder", variables);
            workOrderService.updateWorkOrder(bizExampleDemo);
        } catch (Exception e) {
            e.printStackTrace();
            return error("提交申请出错：" + e.getMessage());
        }
        return success();
    }


}
