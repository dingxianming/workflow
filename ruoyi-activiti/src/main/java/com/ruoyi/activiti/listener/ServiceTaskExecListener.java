package com.ruoyi.activiti.listener;

import lombok.AllArgsConstructor;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 一只闲鹿
 */
@Component
public class ServiceTaskExecListener implements ExecutionListener {

    private TaskService taskService;
    private HistoryService historyService;


    private RuntimeService runtimeService;
    @Override
    public void notify(DelegateExecution execution) {
        //System.out.println("执行了 ServiceTaskExecListener，这里处理【相关人员知会留存】业务");
        List list = new ArrayList<>();
        list.add("rensm");
        execution.setVariable("user","rensm");
    }
}
