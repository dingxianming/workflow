package com.ruoyi.activiti.mapper;

import java.util.List;
import com.ruoyi.activiti.domain.WorkOrder;

/**
 * 工单流程Mapper接口
 * 
 * @author 丁宪明
 * @date 2020-12-22
 */
public interface WorkOrderMapper 
{
    /**
     * 查询工单流程
     * 
     * @param id 工单流程ID
     * @return 工单流程
     */
    public WorkOrder selectWorkOrderById(Long id);

    /**
     * 查询工单流程列表
     * 
     * @param workOrder 工单流程
     * @return 工单流程集合
     */
    public List<WorkOrder> selectWorkOrderList(WorkOrder workOrder);

    /**
     * 新增工单流程
     * 
     * @param workOrder 工单流程
     * @return 结果
     */
    public int insertWorkOrder(WorkOrder workOrder);

    /**
     * 修改工单流程
     * 
     * @param workOrder 工单流程
     * @return 结果
     */
    public int updateWorkOrder(WorkOrder workOrder);

    /**
     * 删除工单流程
     * 
     * @param id 工单流程ID
     * @return 结果
     */
    public int deleteWorkOrderById(Long id);

    /**
     * 批量删除工单流程
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWorkOrderByIds(Long[] ids);
}
