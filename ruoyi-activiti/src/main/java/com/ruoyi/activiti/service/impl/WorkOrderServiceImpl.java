package com.ruoyi.activiti.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.activiti.mapper.WorkOrderMapper;
import com.ruoyi.activiti.domain.WorkOrder;
import com.ruoyi.activiti.service.IWorkOrderService;

/**
 * 工单流程Service业务层处理
 * 
 * @author 丁宪明
 * @date 2020-12-22
 */
@Service
public class WorkOrderServiceImpl implements IWorkOrderService 
{
    @Autowired
    private WorkOrderMapper workOrderMapper;

    /**
     * 查询工单流程
     * 
     * @param id 工单流程ID
     * @return 工单流程
     */
    @Override
    public WorkOrder selectWorkOrderById(Long id)
    {
        return workOrderMapper.selectWorkOrderById(id);
    }

    /**
     * 查询工单流程列表
     * 
     * @param workOrder 工单流程
     * @return 工单流程
     */
    @Override
    public List<WorkOrder> selectWorkOrderList(WorkOrder workOrder)
    {
        return workOrderMapper.selectWorkOrderList(workOrder);
    }

    /**
     * 新增工单流程
     * 
     * @param workOrder 工单流程
     * @return 结果
     */
    @Override
    public int insertWorkOrder(WorkOrder workOrder)
    {
        workOrder.setCreateBy(SecurityUtils.getUsername());
        workOrder.setCreateTime(DateUtils.getNowDate());
        return workOrderMapper.insertWorkOrder(workOrder);
    }

    /**
     * 修改工单流程
     * 
     * @param workOrder 工单流程
     * @return 结果
     */
    @Override
    public int updateWorkOrder(WorkOrder workOrder)
    {
        workOrder.setUpdateTime(DateUtils.getNowDate());

        return workOrderMapper.updateWorkOrder(workOrder);
    }

    /**
     * 批量删除工单流程
     * 
     * @param ids 需要删除的工单流程ID
     * @return 结果
     */
    @Override
    public int deleteWorkOrderByIds(Long[] ids)
    {
        return workOrderMapper.deleteWorkOrderByIds(ids);
    }

    /**
     * 删除工单流程信息
     * 
     * @param id 工单流程ID
     * @return 结果
     */
    @Override
    public int deleteWorkOrderById(Long id)
    {
        return workOrderMapper.deleteWorkOrderById(id);
    }
}
