import request from '@/utils/request'

// 查询工单流程列表
export function listOrder(query) {
  return request({
    url: '/workorder/order/list',
    method: 'get',
    params: query
  })
}

// 查询工单流程详细
export function getOrder(id) {
  return request({
    url: '/workorder/order/' + id,
    method: 'get'
  })
}

// 新增工单流程
export function addOrder(data) {
  return request({
    url: '/workorder/order',
    method: 'post',
    data: data
  })
}

// 修改工单流程
export function updateOrder(data) {
  return request({
    url: '/workorder/order',
    method: 'put',
    data: data
  })
}

// 删除工单流程
export function delOrder(id) {
  return request({
    url: '/workorder/order/' + id,
    method: 'delete'
  })
}

// 导出工单流程
export function exportOrder(query) {
  return request({
    url: '/workorder/order/export',
    method: 'get',
    params: query
  })
}